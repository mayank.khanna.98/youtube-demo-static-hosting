# youtube-demo-static-hosting

## What are Static Files
HTML files - index.html, about.html etc.
- Static Files are nothing but the files that have HTML content and are directly served / deployed.
- When we create REACT / Angular apps - 
	- They take care of the creation of static files.
	- `public/` - directory which contains all the static files
		- `css/`
		- `js/`
		- `img/`
		- `scss/`

---
## What is GitLab / GitHub
- Hub means a collection of same things
- Github - collection of git repos.
- Gitlab - same as GitHub, but with more focus on DevOps / SecOps / CI/CD Pipelines.
- Our code of the website let's say will be hosted on GitLab.
	- It will be deployed for FREE !
	- We will be writing a little code for deployment job
		- `.gitlab-ci.yml`
		- CI - Continuous Integration
		- For example - when we push our code into the repo, it will automatically integrate it with the existing code.
		- CD - Continuous Deployment
		- Once the code is integrated, it needs to be deployed.
	- Avoid as much manual intervention as possible.
		- Less Human Errors
		- Less code inconsistencies. 

---

For full demo visit - [Youtube](https://youtube.com/@devopswithmk)
